const fs = require("fs");
const axios = require("axios");
const Path = require("path");

headers = {
  "X-THN-API-Key": "",
  "X-THN-API-Secret": "",
};

config = {
  downloadImages: false,
};

const skills = {
  riflecombat: "r-c",
  pistolcombat: "p-c",
  techcombat: "t-c",
  heavycombat: "h-c",
  weaponlore: "wep",
  willpower: "wpw",
  transport: "tra",
  psiuse: "psu",
  construction: "cst",
  research: "res",
  implant: "imp",
  focussing: "fcs",
  meleecombat: "m-c",
  athletics: "atl",
  agility: "agl",
  endurance: "end",
  remotecontrol: "rcl",
  repair: "rep",
  recycle: "rec",
  barter: "brt",
  force: "for",
  pierce: "pcr",
  piercing: "pcr",
  energy: "enr",
  fire: "fir",
  xray: "xrr",
  fcs: "fcs",
  poison: "por",
  health: "hlt",
  dex: "dex",
  str: "str",
  int: "int",
  psi: "psi",
  con: "con",
  apu: "apu",
  ppu: "ppu",
  hacking: "hck",
};

const options = {
  url: "https://db.techhaven.org/nc-db/api/v3.0/group-access-list.json",
  headers: headers,
};

const getEverythingFromTHNAPI = async () => {
  const body = await axios(options);

  body.data.granted.forEach(async (type) => {
    const options = {
      url: type.url_json,
      headers: headers,
    };
    const response = await axios(options);
    const newArray = await extractDataFromJson(type, response.data.items);
    try {
      fs.writeFileSync(
        "./export/" + type.name.replace(/\s/g, "").toLowerCase() + ".json",
        JSON.stringify(newArray)
      );
    } catch (e) {
      console.log(e);
    }
  });
};

const download = async (uri, filename) => {
  const path = Path.resolve(__dirname, "export/images", filename.trim());
  const writer = fs.createWriteStream(path);
  const options = {
    url: uri,
    headers: Object.assign(headers, { Referer: "https://neoskiller.com" }),
    responseType: "stream",
  };
  await axios.default(options).then((response) => {
    return new Promise((resolve, reject) => {
      response.data.pipe(writer);
      let error = null;
      writer.on("error", (err) => {
        error = err;
        writer.close();
        reject(err);
      });
      writer.on("close", () => {
        if (!error) {
          resolve(true);
        }
      });
    });
  });
};

const extractDataFromJson = async (type, items) => {
  const allAsyncResults = [];

  for (const item of items) {
    const newObject = {};
    newObject.tl = item.level;
    newObject.name = item.name;
    newObject.type = type.name.toLowerCase();
    newObject.requirements = [];
    newObject.modifiers = [];
    const key = Object.keys(item.detail)[0];
    item.detail[key].attribute.forEach((attribute) => {
      switch (attribute.attributeType) {
        case "requirement":
          newObject.requirements.push({
            shortName: attribute.shortName.toLowerCase(),
            value: (attribute.shortName != 'Class') ? parseInt(attribute.formatValue) : attribute.formatValue,
          });
          break;
        case "modifier":
          newObject.modifiers.push({
            shortName: attribute.shortName.toLowerCase(),
            value: parseInt(attribute.formatValue),
          });
          break;
        case "armour":
          newObject.modifiers.push({
            shortName: skills[attribute.shortName.toLowerCase()],
            value: parseInt(attribute.formatValue),
            type: "armor",
          });
          break;
      }
    }, newObject);

    if (config.downloadImages) {
      await download(
        item.detail[key].image.cdn_url,
        item.detail[key].image.filename
      );
    }

    newObject.image = {
      url: "/assets/images/" + item.detail[key]?.image?.filename,
      x: item.detail[key]?.image?.x,
      y: item.detail[key]?.image?.y,
      name: item.detail[key]?.image?.filename,
    };
    allAsyncResults.push(newObject);
  }

  return allAsyncResults;
};

getEverythingFromTHNAPI();
