start
Reflexbooster v. 0.1
req dex 20
+ dex 1.000
+ agility 6.000
+ athletics 3.000
- int 1.000
- hacking 2.000
- weaponlore 1.000
end

start
Reflexbooster v. 0.2
req dex 40
+ dex 2.000
+ agility 10.000
+ athletics 5.000
- int 2.000
- hacking 3.000
- weaponlore 1.000
end

start
Reflexbooster v. 0.3
req dex 55
+ dex 3.000
+ agility 13.000
+ athletics 6.000
- int 3.000
- hacking 4.000
- weaponlore 2.000
end

start
Reflexbooster v. 0.4
req dex 65
+ dex 4.000
+ agility 15.000
+ athletics 7.000
- int 4.000
- hacking 5.000
- weaponlore 2.000
end

start
Experimental Reflexbooster v. 2.3
req dex 70
+ dex 5.000
+ agility 16.000
+ endurance 10.000
- int 1.000
- hacking 5.000
- weaponlore 1.000
end

start
Experimental Reflexbooster v. 2.4
req dex 78
+ dex 6.000
+ agility 17.000
+ endurance 12.000
- int 2.000
- hacking 6.000
- weaponlore 2.000
end

start
Hardenbackbone v. 1.0
req str 22
+ str 2.000
+ heavycombat 4.000
+ transport 4.000
- dex 1.000
- agility 3.000
+ piercing 4.000
end

start
Hardenbackbone v. 2.0
req str 47
+ str 3.000
+ heavycombat 8.000
+ transport 8.000
- dex 2.000
- agility 5.000
+ piercing 8.000
end

start
Hardenbackbone v. 3.0
req str 72
+ str 5.000
+ heavycombat 11.000
+ transport 11.000
- dex 3.000
- agility 7.000
+ piercing 11.000
end

start
Advanced Nerves v. 1.0
req int 25
+ int 2.000
+ hacking 9.000
+ construction 2.000
+ research 2.000
- con 1.000
- athletics 2.000
- endurance 2.000
+ implant 7.000
end

start
Advanced Nerves v. 2.0
req int 52
+ int 4.000
+ hacking 15.000
+ construction 4.000
+ research 4.000
- con 2.000
- athletics 3.000
- endurance 3.000
+ implant 13.000
end

start
Advanced Nerves v. 3.0
req int 80
+ int 6.000
+ hacking 21.000
+ construction 6.000
+ research 6.000
- con 3.000
- athletics 4.000
- endurance 4.000
+ implant 17.000
end

start
Dexterity Booster v. 1.0
req dex 15
+ dex 1.000
+ agility 7.000
- con 1.000
- endurance 1.000
end

start
Dexterity Booster v. 2.0
req dex 35
+ dex 2.000
+ agility 12.000
- con 2.000
- endurance 2.000
end

start
Dexterity Booster v. 3.0
req dex 64
+ dex 3.000
+ agility 20.000
- con 3.000
- endurance 3.000
end

start
Strength Booster v. 1.0
req str 14
+ str 1.000
+ transport 7.000
- dex 1.000
- agility 2.000
+ meleecombat  5.000
end

start
Strength Booster v. 2.0
req str 34
+ str 2.000
+ transport 12.000
- dex 2.000
- agility 4.000
+ meleecombat  10.000
end

start
Strength Booster v. 3.0
req str 63
+ str 3.000
+ transport 19.000
- dex 3.000
- agility 6.000
+ meleecombat  15.000
end

