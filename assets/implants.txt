start
Experimental MindControl CPU v.2.1
req dex 25
+ dex 1.000
+ remotecontrol 6.000
+ willpower 2.000
- vehicleuse 2.000
- weaponlore 2.000
end

start
Experimental MindControl CPU v.2.2
req dex 54
+ dex 2.000
+ remotecontrol 13.000
+ willpower 4.000
- vehicleuse 3.000
- weaponlore 4.000
end

start
Experimental MindControl CPU v.2.3
req dex 76
+ dex 3.000
+ remotecontrol 17.000
+ willpower 6.000
- vehicleuse 4.000
- weaponlore 6.000
end

start
Drone Distance Interface v. 1.1
req dex 10
+ dex 1.000
+ willpower 5.000
- int 1.000
- weaponlore 2.000
end

start
Drone Distance Interface v. 1.2
req dex 37
+ dex 2.000
+ willpower 12.000
- int 1.000
- weaponlore 4.000
end

start
Drone Distance Interface v. 1.3
req dex 65
+ dex 3.000
+ willpower 19.000
- int 1.000
- weaponlore 7.000
end

start
Crahn PSI Controller v. 0.1
req psi 4
+ psi 1.000
+ psiuse 4.000
- int 1.000
- weaponlore 1.000
end

start
Crahn PSI Controller v. 0.2
req psi 29
+ psi 2.000
+ psiuse 10.000
- int 1.000
- weaponlore 4.000
end

start
Crahn PSI Controller v. 0.3
req psi 54
+ psi 3.000
+ psiuse 16.000
- int 1.000
- weaponlore 6.000
end

start
Crahn PSI Aggressor CPU v. 1.1
req psi 13
+ psi 1.000
+ apu 6.000
+ psiuse 1.000
- ppu 4.000
+ focussing 2.000
end

start
Crahn PSI Aggressor CPU v. 1.2
req psi 38
+ psi 2.000
+ apu 11.000
+ psiuse 2.000
- ppu 9.000
+ focussing 3.000
end

start
Crahn PSI Aggressor CPU v. 1.3
req psi 63
+ psi 3.000
+ apu 16.000
+ psiuse 3.000
- ppu 14.000
+ focussing 4.000
end

start
Crahn PSI Defender CPU v. 1.1
req psi 14
+ psi 1.000
+ ppu 6.000
+ psiuse 4.000
- apu 4.000
+ focussing 5.000
end

start
Crahn PSI Defender CPU v. 1.2
req psi 39
+ psi 2.000
+ ppu 11.000
+ psiuse 7.000
- apu 9.000
+ focussing 9.000
end

start
Crahn PSI Defender CPU v. 1.3
req psi 64
+ psi 3.000
+ ppu 16.000
+ psiuse 10.000
- apu 14.000
+ focussing 13.000
end

start
Crahn Defence Field v. 0.1
req psi 8
+ resistpsi 7.000
- int 1.000
- hacking 1.000
end

start
Crahn Defence Field v. 0.2
req psi 33
+ resistpsi 15.000
- int 1.000
- hacking 3.000
end

start
Crahn Defence Field v. 0.3
req psi 58
+ resistpsi 23.000
- int 2.000
- hacking 4.000
end

start
Crahn PSI Resistor v. 0.1
req psi 12
+ resistpsi 8.000
- int 1.000
- psiuse 2.000
end

start
Crahn PSI Resistor v. 0.2
req psi 37
+ resistpsi 16.000
- int 1.000
- psiuse 3.000
end

start
Crahn PSI Resistor v. 0.3
req psi 62
+ resistpsi 24.000
- int 2.000
- psiuse 5.000
end

start
Experimental Crahn PSI Controller v. 1.01
req psi 25
+ psi 1.000
+ psiuse 5.000
+ focussing 5.000
- techcombat 2.000
- weaponlore 2.000
end

start
Experimental Crahn PSI Controller v. 1.02
req psi 50
+ psi 2.000
+ psiuse 8.000
+ focussing 8.000
- techcombat 3.000
- weaponlore 3.000
end

start
Experimental Crahn PSI Controller v. 1.03
req psi 75
+ psi 3.000
+ psiuse 11.000
+ focussing 11.000
- techcombat 5.000
- weaponlore 5.000
end

start
Targetingcomputer v. 0.01
req dex 6
+ techcombat 4.000
+ weaponlore 2.000
+ dex 1.000
end

start
Targetingcomputer v. 0.02
req dex 33
+ techcombat 9.000
+ weaponlore 6.000
+ dex 2.000
end

start
Targetingcomputer v. 0.03
req dex 61
+ techcombat 14.000
+ weaponlore 9.000
+ dex 3.000
end

start
Experience Melee Memory v. 0.01 Beta
req str 5
+ meleecombat  6.000
- pistolcombat 1.000
- riflecombat 1.000
- techcombat 1.000
end

start
Experience Melee Memory v. 0.02 Beta
req str 30
+ meleecombat  14.000
- pistolcombat 2.000
- riflecombat 2.000
- techcombat 2.000
end

start
Experience Melee Memory v. 0.03 Beta
req str 55
+ meleecombat  22.000
- pistolcombat 3.000
- riflecombat 3.000
- techcombat 3.000
end

start
Long Distance CPU v. 0.1
req dex 10
+ riflecombat 7.000
+ techcombat 1.000
+ weaponlore 1.000
- pistolcombat 1.000
- meleecombat  1.000
end

start
Long Distance CPU v. 0.2
req dex 37
+ riflecombat 16.000
+ techcombat 3.000
+ weaponlore 3.000
- pistolcombat 3.000
- meleecombat  3.000
end

start
Long Distance CPU v. 0.3
req dex 65
+ riflecombat 21.000
+ techcombat 5.000
+ weaponlore 5.000
- pistolcombat 4.000
- meleecombat  4.000
end

start
Synapse Berserk Mod X-11
req str 18
+ str 2.000
+ meleecombat  8.000
- heavycombat 2.000
- endurance 2.000
end

start
Synapse Berserk Mod X-12
req str 43
+ str 3.000
+ meleecombat  14.000
- heavycombat 3.000
- endurance 3.000
end

start
Synapse Berserk Mod X-13
req str 68
+ str 4.000
+ meleecombat  21.000
- heavycombat 4.000
- endurance 4.000
end

start
Vehicle Interface v. 1.1
req dex 23
+ dex 1.000
+ vehicleuse 18.000
- con 1.000
- athletics 3.000
end

start
Vehicle Interface v. 1.2
req dex 49
+ dex 2.000
+ vehicleuse 31.000
- con 1.000
- athletics 5.000
end

start
Vehicle Interface v. 1.3
req dex 62
+ dex 3.000
+ vehicleuse 38.000
- con 1.000
- athletics 6.000
end

start
Hacking Accelerator v. 1.1
req int 23
+ int 1.000
+ hacking 9.000
- psi 1.000
- focussing 3.000
end

start
Hacking Accelerator v. 1.2
req int 50
+ int 2.000
+ hacking 16.000
- psi 1.000
- focussing 5.000
end

start
Hacking Accelerator v. 1.3
req int 78
+ int 3.000
+ hacking 23.000
- psi 1.000
- focussing 8.000
end

start
Construction Coordinator v. 1.0
req int 19
+ int 1.000
+ construction 8.000
- repair 2.000
- recycle 2.000
end

start
Construction Coordinator v. 2.0
req int 46
+ int 2.000
+ construction 15.000
- repair 3.000
- recycle 3.000
end

start
Construction Coordinator v. 3.0
req int 74
+ int 3.000
+ construction 22.000
- repair 5.000
- recycle 5.000
end

start
Advanced Movement Controller v. 1.1
req con 16
+ con 2.000
+ athletics 4.000
+ endurance 9.000
- dex 1.000
end

start
Advanced Movement Controller v. 1.2
req con 42
+ con 4.000
+ athletics 8.000
+ endurance 15.000
- dex 2.000
end

start
Advanced Movement Controller v. 1.3
req con 65
+ con 6.000
+ athletics 11.000
+ endurance 22.000
- dex 3.000
end

start
Balance Advancer v. 1.01
req dex 19
+ dex 1.000
+ agility 8.000
- con 1.000
end

start
Balance Advancer v. 1.02
req dex 32
+ dex 2.000
+ agility 11.000
- con 2.000
end

start
Balance Advancer v. 1.03
req dex 58
+ dex 3.000
+ agility 18.000
- con 3.000
end

start
Experimental Balistic Chip v. 2.1
req dex 26
+ techcombat 6.000
+ weaponlore 8.000
- meleecombat  3.000
- remotecontrol 2.000
+ dex 1.000
end

start
Experimental Balistic Chip v. 2.2
req dex 56
+ techcombat 9.000
+ weaponlore 14.000
- meleecombat  5.000
- remotecontrol 3.000
+ dex 2.000
end

start
Experimental Balistic Chip v. 2.3
req dex 74
+ techcombat 12.000
+ weaponlore 18.000
- meleecombat  6.000
- remotecontrol 4.000
+ dex 3.000
end

start
Marines CPU
graphic sm
req str 81
+ str 4.000
+ transport 16.000
+ force 10.000
+ piercing 10.000
- int 1.000
- hacking 5.000
+ meleecombat  12.000
+ heavycombat 12.000
end

start
Special Forces CPU
graphic sf
req dex 78
+ dex 4.000
+ riflecombat 16.000
+ techcombat 16.000
- str 1.000
- meleecombat  5.000
end

start
Crahn Combat Core CPU
graphic ccc
req psi 82
+ psi 4.000
+ psiuse 16.000
+ apu 8.000
+ focussing 8.000
- ppu 5.000
end

start
Neural Advancement v.0.1
req int 9
+ int 3.000
+ hacking 6.000
end

start
Neural Advancement v.0.2
req int 20
+ int 6.000
+ hacking 13.000
end

start
Neural Advancement v.0.3
req int 28
+ int 9.000
+ hacking 18.000
end

start
Coordination Advancement v.0.1
req dex 11
+ dex 3.000
+ agility 3.000
end

start
Coordination Advancement v.0.2
req dex 20
+ dex 5.000
+ agility 7.000
end

start
Coordination Advancement v.0.3
req dex 29
+ dex 7.000
+ agility 9.000
end

start
Crahn PSI Experience Memory Alpha 1
graphic psiimp
req psi 11
+ psi 3.000
+ focussing 7.000
end

start
Crahn PSI Experience Memory Alpha 2
graphic psiimp
req psi 22
+ psi 6.000
+ focussing 14.000
end

start
Crahn PSI Experience Memory Alpha 3
graphic psiimp
req psi 34
+ psi 7.000
+ focussing 20.000
end

start
Close Combat CPU v.1.0
req dex 7
+ pistolcombat 7.000
+ techcombat 1.000
+ weaponlore 1.000
- riflecombat 1.000
- heavycombat 1.000
end

start
Close Combat CPU v.1.1
req dex 34
+ pistolcombat 15.000
+ techcombat 3.000
+ weaponlore 3.000
- riflecombat 2.000
- heavycombat 2.000
end

start
Close Combat CPU v.1.2
req dex 61
+ pistolcombat 20.000
+ techcombat 5.000
+ weaponlore 5.000
- riflecombat 4.000
- heavycombat 4.000
end

start
Synapse Soldier Mod S-11
req str 16
+ str 1.000
+ heavycombat 7.000
- meleecombat  2.000
- athletics 2.000
end

start
Synapse Soldier Mod S-12
req str 41
+ str 3.000
+ heavycombat 14.000
- meleecombat  3.000
- athletics 3.000
end

start
Synapse Soldier Mod S-13
req str 66
+ str 4.000
+ heavycombat 20.000
- meleecombat  4.000
- athletics 4.000
end

start
Special SWAT Processor
graphic swat
req dex 80
+ dex 4.000
+ pistolcombat 16.000
+ techcombat 16.000
- str 1.000
- heavycombat 5.000
end

start
Crahn Support Core CPU
graphic csc
req psi 85
+ psi 4.000
+ psiuse 16.000
+ ppu 8.000
+ focussing 8.000
- apu 6.000
end

start
MindControl CPU v. 1.1
req dex 23
+ dex 1.000
+ remotecontrol 7.000
+ willpower 2.000
- endurance 2.000
- health 2.000
end

start
MindControl CPU v. 1.2
req dex 50
+ dex 2.000
+ remotecontrol 12.000
+ willpower 4.000
- endurance 3.000
- health 3.000
end

start
MindControl CPU v. 1.3
req dex 78
+ dex 3.000
+ remotecontrol 17.000
+ willpower 6.000
- endurance 5.000
- health 5.000
end

start
Augustus' Knowledge
req int 74
+ int 4.000
+ barter 14.000
+ transport 14.000
- construction 3.000
- research 3.000
end

start
Enhanced Physics CPU v.0.1
req str 13
+ str 3.000
+ transport 9.000
end

start
Enhanced Physics CPU v.0.2
req str 24
+ str 6.000
+ transport 15.000
end

start
Enhanced Physics CPU v.0.3
req str 32
+ str 8.000
+ transport 20.000
end

start
Persistence Advancement CPU v.0.1
req con 10
+ con 2.000
end

start
Persistence Advancement CPU v.0.2
req con 21
+ con 3.000
end

start
Persistence Advancement CPU v.0.3
req con 29
+ con 5.000
end

start
Special Science CPU
graphic ss
req int 84
+ int 4.000
+ construction 8.000
+ research 8.000
+ transport 20.000
- con 1.000
- endurance 4.000
end

start
Special Rigger Interface
graphic sri
req dex 76
+ dex 4.000
+ remotecontrol 16.000
+ willpower 16.000
- health 3.000
- endurance 4.000
end

start
AREA MC5 Hercules CPU
graphic mc5herc
req str 80
+ str 6.000
+ meleecombat  17.000
+ piercing 12.000
+ athletics 12.000
+ health 12.000
- pistolcombat 5.000
- riflecombat 5.000
end

start
AREA MC5 Distance Projector
graphic mc5dip
req dex 90
+ dex 7.000
+ riflecombat 23.000
+ techcombat 12.000
+ weaponlore 12.000
- psiuse 5.000
- focussing 5.000
end

start
AREA MC5 Dimension Splitter
graphic mc5ds
req psi 95
+ psi 6.000
+ psiuse 22.000
+ focussing 22.000
+ psipower 12.000
- int 2.000
- weaponlore 6.000
end

start
AREA MC5 Hawking Chip
graphic mc5hwk
req int 95
+ int 7.000
+ hacking 22.000
+ construction 12.000
+ research 12.000
- dex 2.000
- agility 5.000
end

start
AREA MC5 Riggers Dream
graphic mc5rd
req dex 95
+ dex 7.000
+ techcombat 12.000
+ remotecontrol 22.000
+ willpower 12.000
- con 2.000
- endurance 5.000
end

start
AREA MC5 Balistic CPU
graphic mc5ball
req str 85
+ str 7.000
+ heavycombat 12.000
+ transport 12.000
+ endurance 12.000
+ health 12.000
- dex 2.000
- agility 5.000
end

start
AREA MC5 Close Combat Projector
graphic mc5ccp
req dex 90
+ dex 7.000
+ pistolcombat 22.000
+ techcombat 12.000
+ weaponlore 12.000
- psiuse 4.000
- focussing 4.000
end

start
Experience Memory Barter CPU v. 0.01
req barter 15
+ barter 6.000
end

start
BioTech M.O.V.E.O.N. CPU
req con 35
+ con 5.000
+ endurance 35.000
+ health 19.000
+ transport 21.000
+ resistpsi 15.000
end

start
T-Haven-CPU
req int 35
+ repair 11.000
+ recycle 11.000
+ research 11.000
+ construction 11.000
- techcombat 3.000
- weaponlore 3.000
end

start
ProtoPharm Resistor Chip
graphic ppr
req con 35
+ energy  17.000
+ xray 13.000
+ poison 13.000
+ transport 8.000
end

start
Freedom Fighter CPU
graphic ff
req con 35
+ poison 16.000
+ fire 9.000
+ piercing 9.000
- dex 1.000
- agility 2.000
end

start
Advanced Tactical Mercenary CPU
graphic atm
req con 35
+ force 16.000
+ fire 9.000
+ poison 9.000
- dex 1.000
- agility 2.000
end

start
NCPD Special Unit CPU
graphic ncpd
req con 35
+ energy  16.000
+ xray 9.000
+ poison 9.000
- dex 1.000
- agility 2.000
end

