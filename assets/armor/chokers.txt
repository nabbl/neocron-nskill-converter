start
Light Kevlar Choker
req str 10
+ armour force 20.000
+ armour pierce 14.000
+ armour fire 17.000
+ armour poison 30.000
end

start
Medium Kevlar Choker
req str 22
+ armour force 25.000
+ armour pierce 17.000
+ armour fire 21.000
+ armour poison 38.000
end

start
Heavy Kevlar Choker
req str 34
+ armour force 30.000
+ armour pierce 19.000
+ armour fire 25.000
+ armour poison 45.000
end

start
Battle Collar Prototype
req str 15
+ armour force 51.000
+ armour pierce 36.000
+ armour fire 20.000
end

start
Usual Battle Collar
req str 25
+ armour force 61.000
+ armour pierce 42.000
+ armour fire 23.000
end

start
Advanced Battle Collar
req str 35
+ armour force 71.000
+ armour pierce 49.000
+ armour fire 26.000
end

start
Shabby Carbon Choker
req str 41
+ armour force 44.000
+ armour pierce 54.000
+ armour poison 29.000
end

start
Good Carbon Choker
req str 49
+ armour force 48.000
+ armour pierce 59.000
+ armour poison 32.000
end

start
Great Carbon Choker
req str 57
+ armour force 52.000
+ armour pierce 64.000
+ armour poison 34.000
end

start
Outstanding Carbon Choker
req str 67
+ armour force 57.000
+ armour pierce 71.000
+ armour poison 38.000
end

start
Bad Duranium Collar
req str 68
+ armour force 67.000
+ armour pierce 52.000
+ armour energy 52.000
end

start
Duranium Collar
req str 78
+ armour force 74.000
+ armour pierce 56.000
+ armour energy 56.000
end

start
Advanced Duranium Collar
req str 83
+ armour force 77.000
+ armour pierce 59.000
+ armour energy 59.000
end

start
High Density Duranium Collar
req str 88
+ armour force 80.000
+ armour pierce 61.000
+ armour energy 61.000
end

start
Unhallowed Inquisition Necklace
req str 39
+ armour force 34.000
+ armour pierce 34.000
+ armour fire 64.000
end

start
Inquisition Necklace
req str 47
+ armour force 37.000
+ armour pierce 37.000
+ armour fire 71.000
end

start
Holy Inquisition Necklace
req str 55
+ armour force 40.000
+ armour pierce 40.000
+ armour fire 77.000
end

start
Gods Wish' Inquisition Necklace
req str 67
+ armour force 45.000
+ armour pierce 45.000
+ armour fire 87.000
end

start
Crahn Shelter Necklace
req psi 6
+ armour force 15.000
+ armour pierce 15.000
+ armour fire 15.000
+ armour energy 26.000
end

start
Crahn Blessed Shelter Necklace
req psi 26
+ armour force 21.000
+ armour pierce 21.000
+ armour fire 21.000
+ armour energy 37.000
end

start
Crahn Spirit Necklace
req psi 49
+ armour force 27.000
+ armour pierce 27.000
+ armour energy 50.000
+ armour xray 27.000
+ armour fire 16.000
end

start
Crahn Blessed Spirit Necklace
req psi 75
+ armour force 34.000
+ armour pierce 34.000
+ armour energy 64.000
+ armour xray 34.000
+ armour fire 19.000
end

